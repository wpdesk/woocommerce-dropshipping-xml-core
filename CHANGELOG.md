## [1.9.3] - 2025-01-21
### Fixed
- tags separator

## [1.9.2] - 2025-01-20
### Fixed
- translations

## [1.9.1] - 2025-01-20
### Fixed
- allow to remove product tags and gpsr fields

## [1.9.0] - 2025-01-10
### Added
- Product tags mapper
- Integration with GPSR for WooCommerce
- Log view
- Single line attributes support
- Custom id feature

## [1.8.5] - 2024-12-03
### Fixed
- Variations cleaner

## [1.8.4] - 2024-11-21
### Fixed
- EAN Meta key

## [1.8.3] - 2024-09-24
### Fixed
- EAN Meta key

## [1.8.2] - 2024-09-19
### Added
- Integration with WC 9.2

## [1.8.1] - 2024-09-09
### Fixed
- Remove deprecation error in php 8.3

## [1.8.0] - 2024-08-23
### Added
- Integration with flexible ean plugin

## [1.7.41] - 2024-08-09
### Added
- New logger

## [1.7.40] - 2024-08-05
### Fixed
- CSS style on products page

## [1.7.39] - 2024-08-05
### Fixed
- Dropshipping column view on products page

## [1.7.38] - 2024-06-20
### Added
- Filter for sale price value

## [1.7.37] - 2024-04-16
### Added
- Filter for conditional logic, lower and higher values

## [1.7.36] - 2024-04-10
### Fixed
- Cleaning products updated by two different imports
### Added
- Parent category filter

## [1.7.35] - 2024-03-26
### Fixed
- Cleaner

## [1.7.34] - 2024-03-25
### Added
- Price mapper xpath support for values

## [1.7.33] - 2024-03-21
### Fixed
- Issues with xml analyser

## [1.7.32] - 2024-03-13
### Fixed
- Issues with XML parsed content

## [1.7.31] - 2024-02-27
### Fixed
- translation file

## [1.7.30] - 2024-02-27
### Fixed
- translations

## [1.7.29] - 2024-02-26
### Fixed
- translations

## [1.7.28] - 2024-02-26
### Fixed
- deprecated notices

## [1.7.27] - 2024-02-14
### Fixed
- cleaner

## [1.7.26] - 2024-02-14
### Fixed
- cleaner

## [1.7.25] - 2024-02-09
### Changed
- translations

## [1.7.24] - 2023-11-14
### Fixed
- error notice logs
### Added
- image filter

## [1.7.23] - 2023-10-11
### Fixed
- menu item color

## [1.7.22] - 2023-10-16
### Fixed
- support urls

## [1.7.21] - 2023-09-14
### Fixed
- logo icon

## [1.7.20] - 2023-09-11
### Fixed
- price mapper

## [1.7.19] - 2023-09-11
### Fixed
- regular price updater

## [1.7.18] - 2023-09-11
### Fixed
- sale price updater
## [1.7.17] - 2023-09-11
### Fixed
- sale price updater

## [1.7.16] - 2023-09-07
### Fixed
- marketing box assets

## [1.7.15] - 2023-09-07
### Fixed
- remove temp files functionality

## [1.7.14] - 2023-09-06
### Added
- cron task to remove temp files

## [1.7.13] - 2023-08-24
### Added
- new filters

## [1.7.12] - 2023-08-03
### Fixed
- empty Attributes error 

## [1.7.11] - 2023-07-20
### Added
- embedded variation filter
## [1.7.10] - 2023-07-19
### Fixed
- contains condition

## [1.7.9] - 2023-07-17
### Added
- filters
## [1.7.8] - 2023-07-14
### Added
- filters
## [1.7.7] - 2023-07-11
### Fixed
- finding product drafts
## [1.7.6] - 2023-07-10
### Fixed
- Cleaner issues
## [1.7.5] - 2023-07-06
### Fixed
- Filter typo
## [1.7.4] - 2023-07-06
### Fixed
- Price groups modificator
### Added
- categories and images filters 
## [1.7.3] - 2023-07-04
### Fixed
- Price converter
## [1.7.2] - 2023-06-21
### Added
- Price modificator coverter
## [1.7.1] - 2023-06-16
### Added
- Price modificator for embedded variations
## [1.7.0] - 2023-06-15
### Added
- Conditional logic for price
## [1.6.5] - 2023-05-30
### Fixed
- Product cleaner service for product variations
## [1.6.4] - 2023-05-22
### Changed
- Removed placeholder from category tree separator field
## [1.6.3] - 2023-05-22
### Fixed
- Categories mapper
## [1.6.2] - 2023-05-10
### Added
- Categories tree creator
## [1.6.1] - 2023-05-10
### Added
- Categories tree creator
## [1.6.0] - 2023-04-13
### Added
- WPDesk tracker
## [1.5.14] - 2023-04-03
### Added
- add filter wpdesk_dropshipping_mapper_weight
## [1.5.13] - 2023-02-14
### Fixed
- add monolog and wp-logs libraries
## [1.5.12] - 2023-02-13
### Fixed
- parent options for embedded variations
## [1.5.11] - 2023-01-26
### Fixed
- price calculation
## [1.5.10] - 2023-01-02
### Fixed
- product attributes error 
## [1.5.9] - 2022-10-25
### Fixed
- tax mapper
## [1.5.8] - 2022-10-13
### Fixed
- product images gallery
- product variations stock mapper
## [1.5.7] - 2022-09-22
### Fixed
- product mapper view
- price for embedded variations
- translations
## [1.5.6] - 2022-08-18
### Fixed
- import creator error
## [1.5.5] - 2022-08-18
### Fixed
- import creator
## [1.5.4] - 2022-08-18
### Fixed
- translations
## [1.5.3] - 2022-08-18
### Fixed
- clone functionality
## [1.5.2] - 2022-08-18
### Fixed
- help descriptions
## [1.5.1] - 2022-08-17
### Fixed
- help descriptions
## [1.5.0] - 2022-08-10
### Added
- option to add import name
- clone import feature
- how to use page
## [1.4.5] - 2022-06-27
### Fixed
- removed parent regular price and sale price from embedded products variations
## [1.4.4] - 2022-05-31
### Added
- add raw price parameter to price filter
## [1.4.3] - 2022-05-23
### Fixed
- encode error in csv file
## [1.4.2] - 2022-05-23
### Fixed
- invalid field name
## [1.4.1] - 2022-05-23
### Fixed
- translations
## [1.4.0] - 2022-05-23
### Added
- option to append images to the gallery
### Fixed
- path to assets on windows servers
## [1.3.18] - 2022-05-06
### Fixed
- variations mapper
## [1.3.17] - 2022-05-05
### Added
- wpdesk_dropshipping_csv_encode_string filter
## [1.3.16] - 2022-04-05
### Fixed
- variations preview
## [1.3.15] - 2022-04-05
### Changed
- Update libs
## [1.3.14] - 2022-04-05
### Fixed
- images creation for unmaped product categories
## [1.3.13] - 2022-02-25
### Fixed
- changed styling
- translation
- conditional logic for higher and lower values 
## [1.3.12] - 2022-02-22
### Fixed
- cutting out invalid xml characters when converting from csv
## [1.3.11] - 2022-02-22
### Fixed
- allow simple product to update variation data
## [1.3.10] - 2022-01-19
### Added
- price filters for variations
## [1.3.9] - 2022-01-18
### Added
- price filters
## [1.3.8] - 2022-01-11
### Fixed
- remove stopped status from processing import
## [1.3.7] - 2022-01-05
### Fixed
- csv file encoding
### Fixed
- embedded variations selector
## [1.3.6] - 2022-01-05
### Added
- supported mime types filter
### Fixed
- embedded variations selector
## [1.3.5] - 2021-12-20
### Fixed
- add has parent meta key to embedded variations
## [1.3.4] - 2021-12-09
### Fixed
- attributes for variations
## [1.3.3] - 2021-12-01
### Fixed
- issue with xml preview pagination
## [1.3.2] - 2021-12-01
### Fixed
- variations as virtual products
## [1.3.1] - 2021-11-29
### Fixed
- variations popup size
- translations
## [1.3.0] - 2021-11-23
### Added
- support for embedded variations
## [1.2.3] - 2021-10-21
### Fixed
- Translations
## [1.2.2] - 2021-10-20
### Fixed
- Url escaping
## [1.2.1] - 2021-10-19
### Fixed
- Missing translations
## [1.2.0] - 2021-10-19
### Added
- External product type
- option to remove the featured image from gallery
## [1.1.0] - 2021-10-11
### Added
- set credentials method on curl options
### Fixed
- Double slashes in assets
- Replace bcadd method on logical conditions
## [1.0.18] - 2021-10-05
### Fixed
- Time schedule for next import
## [1.0.17] - 2021-10-05
### Fixed
- Logical conditions for lower and higher values
## [1.0.16] - 2021-10-04
### Changed
- Form templates render method
## [1.0.15] - 2021-09-28
### Fixed
- Form start templates
## [1.0.14] - 2021-09-28
### Fixed
- Escaping functions in templates
## [1.0.13] - 2021-09-24
### Fixed
- variations synchronization
## [1.0.12] - 2021-09-20
### Changed
- remove unused header template
## [1.0.11] - 2021-09-07
### Fixed
- checking instance of the object in dependency resolver
## [1.0.10] - 2021-09-07
### Fixed
- add missing arguments to dependency resolver
## [1.0.9] - 2021-09-07
### Fixed
- dependecy resolver
## [1.0.8] - 2021-09-07
### Changed
- replaced include with renderer 
## [1.0.7] - 2021-09-02
### Changed
- translations
## [1.0.6] - 2021-09-02
### Changed
- MIME type for CSV file
### Fixed
- csv file encoding

## [1.0.5] - 2021-08-31
### Changed
- updated wp-forms version in composer.json
## [1.0.4] - 2021-08-20
### Changed
- change MapperServiceFactory methods to protected
## [1.0.3] - 2021-08-20
### Changed
- access methods in images mapper
## [1.0.2] - 2021-08-20
### Changed
- method to get images

## [1.0.1] - 2021-08-19
### Fixed
- text-domain
- translations
## [1.0] - 2021-08-18
### Added
- init
