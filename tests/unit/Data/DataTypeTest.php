<?php

use WPDesk\Library\DropshippingXmlCore\Infrastructure\Data\DataType;

class DataTypeTest extends \PHPUnit\Framework\TestCase {

	const TEST_STRING = 'some string';
	const TEST_EMPTY_STRING = '';
	const TEST_INT = 666;
	const TEST_ZERO = 0;
	const TEST_FLOAT = 5.5;
	const TEST_URL = 'https://www.wpdesk.pl/';
	const TEST_NULL = null;
	const TEST_ARRAY = ['key1' => 4, 'key2' => 'string', 'key3' => 5.5, 'key4' => array( 1,2,3 )];
	const TEST_BOOL = false;

	private $object;

	private function get_test_object(){
		if(isset($this->object)){
			return $this->object;
		}
		$this->object = new stdClass();
		$this->object->id = self::TEST_INT;
		return $this->object;
	}

	public function data_for_tests() {
		$object = new stdClass();
		$object->id = self::TEST_INT;

		return [
			[ self::TEST_STRING ],
			[ self::TEST_EMPTY_STRING ],
			[ self::TEST_INT ],
			[ self::TEST_ZERO ],
			[ self::TEST_FLOAT ],
			[ self::TEST_URL ],
			[ self::TEST_NULL ],
			[ self::TEST_ARRAY ],
			[ $this->get_test_object() ],
			[ self::TEST_BOOL ]
		];
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_isset( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_NULL ){
			$this->assertFalse( $data_type->isSet() );
		}else{
			$this->assertTrue( $data_type->isSet() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_empty( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_NULL || $value === self::TEST_EMPTY_STRING || $value === self::TEST_ZERO || $value === self::TEST_BOOL  ){
			$this->assertTrue( $data_type->isEmpty() );
		}else{
			$this->assertFalse( $data_type->isEmpty() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_string( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_EMPTY_STRING || $value === self::TEST_STRING || $value === self::TEST_URL  ){
			$this->assertTrue( $data_type->isString() );
		}else{
			$this->assertFalse( $data_type->isString() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_integer( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_ZERO || $value === self::TEST_INT  ){
			$this->assertTrue( $data_type->isInteger() );
		}else{
			$this->assertFalse( $data_type->isInteger() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_float( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_FLOAT  ){
			$this->assertTrue( $data_type->isFloat() );
		}else{
			$this->assertFalse( $data_type->isFloat() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_numeric( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_FLOAT || $value === self::TEST_INT || $value === self::TEST_ZERO  ){
			$this->assertTrue( $data_type->isNumeric() );
		}else{
			$this->assertFalse( $data_type->isNumeric() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_array( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_ARRAY  ){
			$this->assertTrue( $data_type->isArray() );
		}else{
			$this->assertFalse( $data_type->isArray() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_object( $value ) {
		$data_type = new DataType( $value );

		if( is_object($value) ){
			$this->assertTrue( $data_type->isObject() );
		}else{
			$this->assertFalse( $data_type->isObject() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_url( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_URL ){
			$this->assertTrue( $data_type->isUrl() );
		}else{
			$this->assertFalse( $data_type->isUrl() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_no_serialized( $value ) {
		$data_type = new DataType( $value );

		$this->assertFalse( $data_type->isSerialized() );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_serialized( $value ) {
		$data_type = new DataType( serialize($value) );

		$this->assertTrue( $data_type->isSerialized() );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_no_json( $value ) {
		$data_type = new DataType( $value );

		if( $value === self::TEST_FLOAT || $value === self::TEST_INT || $value === self::TEST_ZERO  ){
			$this->assertTrue( $data_type->isJson() );
		}else{
			$this->assertFalse( $data_type->isJson() );
		}
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_is_json( $value ) {
		$data_type = new DataType( json_encode($value) );
		$this->assertTrue( $data_type->isJson() );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_get( $value ) {
		$data_type = new DataType( $value );
		$this->assertEquals( $value, $data_type->get() );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_get_as_string( $value ) {
		$data_type = new DataType( $value );
		$this->assertTrue( is_string( $data_type->getAsString() ) );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_get_as_json( $value ) {
		$data_type = new DataType( $value );
		json_decode( $data_type->getAsJson() );
		$this->assertTrue( ( json_last_error() === JSON_ERROR_NONE ) );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_get_serialized( $value ) {
		$data_type = new DataType( $value );
		$this->assertEquals( serialize( $value ), $data_type->getSerialized() );
	}

	/**
	 * @dataProvider data_for_tests
	 */
	public function test_to_string( $value ) {
		$data_type = new DataType( $value );
		ob_start();
		echo($data_type);
		$output = ob_get_contents();
		ob_end_clean();
		$this->assertTrue( is_string( $output ) );
	}

}
