const path = require('path');

module.exports = {
	entry: './js/main.js',
	output: {
		path: path.resolve(__dirname, '../assets/js'),
		filename: 'admin.js'
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-env"]
					}
				}
			}
		]
	}
};
