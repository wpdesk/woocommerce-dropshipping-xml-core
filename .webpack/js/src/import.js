"use strict";

/**
 * Import.
 *
 * @param {Object} options
 * @constructor
 */
export default function Import(options) {

	let settings = {
		log_view_selector: '#log-viewer',
		created_selector: '#import-created',
		updated_selector: '#import-updated',
		skipped_selector: '#import-skipped',
		total_selector: '#import-total',
		progress_selector: '#import-progress',
		player_selector: '#import-player',
		stop_selector: '#stop-import > a',
		start_selector: '#start-import > a',
		callback: null,
		notice: new ImportNotice(),
		timer: new Timer(),
		request: new BrowserRequest(),
		exception: new Exception()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.log_view = jQuery(settings.log_view_selector);
	this.created = jQuery(settings.created_selector);
	this.updated = jQuery(settings.updated_selector);
	this.skipped = jQuery(settings.skipped_selector);
	this.total = jQuery(settings.total_selector);
	this.progress = jQuery(settings.progress_selector);
	this.stop_import = jQuery(settings.stop_selector);
	this.start_import = jQuery(settings.start_selector);
	this.notice = settings.notice;
	this.timer = settings.timer;
	this.callback = settings.callback;
	this.request = settings.request;
	this.exception = settings.exception;
	this.player_selector = settings.player_selector;
}

Import.prototype = {
	request: null,
	log_view: null,
	created: null,
	updated: null,
	skipped: null,
	total: null,
	progress: null,
	notice: null,
	timer: null,
	callback: null,
	exception: null,
	stop_import: null,
	start_import: null,
	player_selector: null,
	stop: false,
	import_process: function () {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'import_products',
				uid: self.request.getUid(),
				security: import_products.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.add_to_log(data.log);
					self.update_created(parseInt(data.created));
					self.update_updated(parseInt(data.updated));
					self.update_skipped(parseInt(data.skipped));
					self.update_total(parseInt(data.total));
					self.update_progress(data.progress);
					if (data.finished === true || self.stop === true ) {
						self.timer.stop_timer();
						if (typeof self.callback === "function") {
							self.callback();
						}
					} else {
						self.import_process();
					}
				} else {
					self.notice.error_notice(data.message);
				}
			},
			error: function ( jqXHR, exception ) {
				self.notice.error_notice(self.exception.get_message( jqXHR, exception ));
			}
		});
	},

	/**
	 * @param {integer} num.
	 */
	update_created: function (num) {
		this.created.text(num);
	},

	/**
	 * @param {integer} num.
	 */
	update_updated: function (num) {
		this.updated.text(num);
	},

	/**
	 * @param {integer} num.
	 */
	update_skipped: function (num) {
		this.skipped.text(num);
	},

	/**
	 * @param {integer} num.
	 */
	update_total: function (num) {
		this.total.text(num);
	},

	/**
	 * @param {string} content.
	 */
	add_to_log: function (content) {
		this.log_view.val(this.log_view.val() + content);
	},

	/**
	 * @param {string} progress.
	 */
	update_progress: function (progress) {
		this.progress.text(progress);
	},
	stop_import_process: function () {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'stop_import_products',
				uid: self.request.getUid(),
				security: stop_import_products.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.add_to_log(data.message + "\n");
				} else {
					self.notice.error_notice(data.message);
				}
			},
			error: function ( jqXHR, exception ) {
				self.notice.error_notice(self.exception.get_message( jqXHR, exception ));
			}
		});
	},

	/**
	 * @param {bool} bool.
	 */
	autostart: function (bool) {
		if (bool === true) {
			this.add_to_log('Import Start...' + "\n");
			this.import_process();
			this.timer.start_timer();
		}
	},
	add_listeners: function () {
		const self = this;
		self.stop_import.on('click', function (e) {
			e.preventDefault();
			self.stop = true;
			jQuery(this).closest(self.player_selector).removeClass('start').addClass('stop');
			self.stop_import_process();
		});

		self.start_import.on('click', function (e) {
			e.preventDefault();
			self.stop = false;
			jQuery(this).closest(self.player_selector).removeClass('stop').addClass('start');
			self.autostart(true);
		});

	},
	init: function () {
		this.add_listeners();
	}
}
