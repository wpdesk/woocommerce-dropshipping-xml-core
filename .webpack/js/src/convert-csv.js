"use strict";

/**
 * Convert CSV.
 *
 * @param {Object} options
 * @constructor
 */
export default function ConvertCsv(options) {

	let settings = {
		button_selector: '#dropshipping-item-separator-button',
		separator_selector: '#dropshipping-item-separator',
		loader_selector: '#dropshipping-table-loader',
		notice: new ImportNotice(),
		callback: null,
		request: new BrowserRequest(),
		exception: new Exception()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.apply_button = jQuery(settings.button_selector);
	this.separator = jQuery(settings.separator_selector);
	this.loader = jQuery(settings.loader_selector);
	this.notice = settings.notice;
	this.callback = settings.callback;
	this.request = settings.request;
	this.exception = settings.exception;
}

ConvertCsv.prototype = {
	apply_button: null,
	separator: null,
	loader: null,
	notice: null,
	callback: null,
	request: null,
	exception: null,
	evaluate: function () {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'evaluate_csv_product_data',
				separator: self.separator.val(),
				uid: self.request.getUid(),
				security: evaluate_csv_product_data.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.notice.success_notice(data.message);
					self.separator.val(data.separator);
					self.apply_button.attr('disabled', false);
					if (typeof self.callback === "function") {
						self.callback(data.items);
					}
					self.loader.hide();
				} else {
					self.apply_button.attr('disabled', false);
					self.notice.error_notice(data.message);
					self.loader.hide();
				}
			},
			error: function (jqXHR, exception) {
				self.apply_button.attr('disabled', false);
				self.notice.error_notice(self.exception.get_message(jqXHR, exception));
				self.loader.hide();
			}
		});
	},
	add_listeners: function () {
		const self = this;
		self.apply_button.on('click', function (e) {
			e.preventDefault();
			self.apply_button.attr('disabled', true);
			self.loader.show();
			self.notice.hide_notice();
			self.evaluate();
		})
	},
	init: function () {
		this.add_listeners();
	}
}

