"use strict";

/**
 * Multi Form Field.
 *
 * @param {Object} options.
 * @constructor
 */
export default function MultiFormField(options) {

	let settings = {
		button_selector: '#button',
		template: null,
		single_item_class: '.single-item',
		wrapper_selector: '#wrapper',
		remove_button_selector: '.remove-button',
		callback: null,
		before_append_callback: null
	}
	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.template = settings.template;
	this.wrapper_selector = settings.wrapper_selector;
	this.button_selector = settings.button_selector;
	this.remove_selector = settings.remove_button_selector;
	this.callback = settings.callback;
	this.before_append_callback = settings.before_append_callback;
	this.single_item_class = settings.single_item_class;
}

MultiFormField.prototype = {
	template: null,
	wrapper_selector: null,
	button_selector: null,
	remove_selector: null,
	single_item_class: null,
	callback: null,
	before_append_callback: null,
	add_new_item: function ( $wrapper ) {
		let new_item = this.template.clone();
		new_item.find('input').val('');
		new_item.find('select').prop('selectedIndex',0);
		new_item.find(this.remove_selector).removeClass('hidden');
		if (typeof this.before_append_callback === "function") {
			new_item = this.before_append_callback(new_item, $wrapper);
		}
		$wrapper.append(new_item);
		this.remove_listener(new_item);
		if (typeof this.callback === "function") {
			this.callback(new_item, $wrapper);
		}
	},

	/**
	 * @param {object} listener.
	 */
	remove_listener: function (listener) {
		const self = this;
		listener.find(this.remove_selector + ':first').on('click', function (e) {
			e.preventDefault();
			jQuery(this).closest(self.single_item_class).remove();
		});
	},
	add_listeners: function () {
		const self = this;
		jQuery(document).on('click', self.button_selector, function (e) {
			e.preventDefault();
			let wrapper = null;
			if (jQuery(this).closest(self.wrapper_selector).length > 0) {
				wrapper = jQuery(this).closest(self.wrapper_selector);
			} else { 
				wrapper = jQuery(self.wrapper_selector);
			}
			self.add_new_item( wrapper );
		});

	},
	init: function () {
		this.add_listeners();
	}
}
