"use strict";

/**
 * BrowserRequest.
 * @constructor
 */
export default function BrowserRequest(){}

BrowserRequest.prototype = {

	/**
	 * @returns {string}.
	 */
	getUid: function () {
		const urlParams = new URLSearchParams(window.location.search);
		if (urlParams.has('uid')) {
			return urlParams.get('uid');
		}
		return '';
	}
}
