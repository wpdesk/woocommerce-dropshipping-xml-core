"use strict";

/**
 * Preview CSV.
 *
 * @param {Object} options.
 * @constructor
 */
export default function ImportNotice(options) {

	let settings = {
		notice_selector: "#dropshipping-notice"
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.notice_selector = settings.notice_selector;
}

ImportNotice.prototype = {
	notice_selector: null,
	hide_notice: function () {
		let $dropshippingNotice = jQuery(document).find(this.notice_selector);
		if ($dropshippingNotice.length) {
			$dropshippingNotice.hide('fast');
		}
	},

	/**
	 * @param {string} message.
	 */
	error_notice: function (message) {
		let $template = this.get_notice_template('notice notice-error');
		$template.html(jQuery('<p>').html(message));
	},

	/**
	 * @param {string} message.
	 */
	success_notice: function (message) {
		let $template = this.get_notice_template('notice notice-success');
		$template.html(jQuery('<p>').html(message));
	},

	/**
	 * @param {string} classes.
	 * return {object}.
	 */
	get_notice_template: function (classes) {
		let $dropshippingNotice = jQuery(document).find(this.notice_selector);
		if ($dropshippingNotice.length) {
			$dropshippingNotice.attr("class", classes);
			return $dropshippingNotice.show('fast');
		}

		$dropshippingNotice = jQuery('<div>').attr("class", classes).attr('id', this.notice_selector.replace(/#/g, ''));

		$dropshippingNotice.insertAfter('.wp-header-end').hide().show('fast');
		return $dropshippingNotice;
	}
}
