"use strict";

/**
 * Droppable Elements.
 *
 * @param {Object} options.
 * @constructor
 */
export default function DraggableElements (options) {

	let settings = {
		classes: ['.simpleXML-attrName', '.simpleXML-tagValue', '.draggable']
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.classes = settings.classes;
}

DraggableElements.prototype = {
	classes: null,
	refresh: function () {
		if (Array.isArray(this.classes) && this.classes.length > 0) {
			this.classes.forEach(function (item, index) {
				jQuery(item).draggable({
					iframeFix: true,
					revert: true,
					appendTo: 'body',
					scroll: false,
					helper: 'clone'
				});
			});
		}
	},
	init: function () {
		this.refresh();
	}
}
