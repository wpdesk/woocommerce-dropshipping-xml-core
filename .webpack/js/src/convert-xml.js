"use strict";

/**
 * Convert XML.
 *
 * @param {Object} options
 * @constructor
 */
export default function ConvertXml(options) {

	let settings = {
		buttons_selector: '.dropshipping-preview-item-button',
		loader_selector: '#dropshipping-table-loader',
		notice: new ImportNotice(),
		callback: null,
		request: new BrowserRequest(),
		exception: new Exception()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.buttons = jQuery(settings.buttons_selector);
	this.loader = jQuery(settings.loader_selector);
	this.notice = settings.notice;
	this.callback = settings.callback;
	this.request = settings.request;
	this.exception = settings.exception;
}

ConvertXml.prototype = {
	buttons: null,
	loader: null,
	notice: null,
	callback: null,
	request: null,
	exception: null,

	/**
	 * @param {string} element_id.
	 */
	evaluate: function (element_id) {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'evaluate_xml_product_data',
				node_element: element_id,
				uid: self.request.getUid(),
				security: evaluate_xml_product_data.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.notice.success_notice(data.message);
					if (typeof self.callback === "function") {
						self.callback(data.items, element_id, data.content);
					}
					self.loader.hide();
				} else {
					self.notice.error_notice(data.message);
					self.loader.hide();
				}
			},
			error: function (jqXHR, exception) {
				self.notice.error_notice(self.exception.get_message(jqXHR, exception));
				self.loader.hide();
			}
		});
	},
	add_listeners: function () {
		const self = this;
		self.buttons.on('click', function (e) {
			e.preventDefault();
			self.loader.show();
			self.notice.hide_notice();
			self.buttons.removeClass('active');
			jQuery(this).addClass('active');
			self.evaluate(jQuery(this).attr('data-id'));
		});
	},
	init: function () {
		this.add_listeners();
	}
}
