"use strict";

/**
 * Droppable Elements.
 *
 * @param {Object} options.
 * @constructor
 */
export default function DroppableElements(options) {

	let settings = {
		selector: 'input[type=text],textarea,#postdivrich,#postexcerpt',
		exclude_ids: ['variation-popup']
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.selector = settings.selector;
	this.exclude_ids = settings.exclude_ids;
}

DroppableElements.prototype = {
	selector: null,
	exclude_ids: null,
	refresh_element: function () {
		let self = this;
			jQuery(document).find(this.selector).droppable({
				drop: function (event, ui) {					
					if (self.exclude_ids.indexOf(ui.draggable.attr('id')) >= 0) {
						console.log('droppable');
						console.log(ui.draggable.attr('id'));
						console.log(self.exclude_ids);
						return false;
					}

					let name = '{' + ui.draggable.attr('xpath') + '}';
					if (typeof tinymce !== 'undefined') {
						let tinymceEditor = tinymce.get(jQuery(this).attr('data-id'));
						if (tinymceEditor != null) {
							tinymceEditor.selection.select(tinymceEditor.getBody(), true);
							tinymceEditor.selection.collapse(false);
							tinymceEditor.execCommand('mceInsertContent', false, name);
						} else {
							jQuery(this).val(jQuery(this).val() + name);
						}
					} else {
						jQuery(this).val(jQuery(this).val() + name);
					}
				}
			});
		},
		init: function () {
			this.refresh_element();
		}
}
