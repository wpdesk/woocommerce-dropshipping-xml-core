"use strict";

jQuery.noConflict();
(function ($) {
	$(function () {
		var DropshippingAdmin = {
			droppable: null,
			draggable: null,
			product_attributes: null,
			variation_attributes: null,
			logical_conditions: null,
			price_mod_conditions: null,
			groups: null,
			product_categories: null,
			product_tax_classes: null,
			variation_tax_classes: null,
			downloader: null,
			xml_view: null,
			xml_variation_view: null,
			preview_csv: null,
			preview_xml: null,
			preview_variation_xml: null,
			navigator: null,
			variation_navigator: null,
			csv_converter: null,
			xml_converter: null,
			import: null,
			fixed_sidebar: null,
			variation_popup: null,
			init_droppable: function () {
				this.droppable = new DroppableElements();
				this.droppable.init();
			},
			init_draggable: function () {
				this.draggable = new DraggableElements();
				this.draggable.init();
			},
			init_product_attributes: function () {
				let self = this;
				self.product_attributes = new MultiFormField({
					button_selector: '#add_attribute',
					template: jQuery('#product_attributes .single-item:first'),
					wrapper_selector: '#attributes-wrapper',
					callback: function () {
						self.droppable.refresh_element();
					}
				});
				self.product_attributes.init();
				if (jQuery("#product_attributes .single-item").length > 0) {
					jQuery("#product_attributes .single-item").each(function (index) {
						self.product_attributes.remove_listener(jQuery(this));
					});
				}
			},
			init_variation_attributes: function () {
				let self = this;
				self.variation_attributes = new MultiFormField({
					button_selector: '#add_variation_attribute',
					template: jQuery('#variation-attributes-wrapper .single-item:first'),
					wrapper_selector: '#variation-attributes-wrapper',
					callback: function () {
						self.droppable.refresh_element();
					}
				});
				self.variation_attributes.init();
				if (jQuery("#variation-attributes-wrapper .single-item").length > 0) {
					jQuery("#variation-attributes-wrapper .single-item").each(function (index) {
						self.variation_attributes.remove_listener(jQuery(this));
					});
				}
			},
			init_logical_conditions: function () {
				let self = this;
				let row = jQuery('#dropshipping-logical-conditions').closest('tr');
				jQuery('#turn_logical_condition').on('change', function () {
					if (jQuery(this).is(':checked')) {
						row.removeClass('hidden');
					} else {
						row.addClass('hidden');
					}
				});
				jQuery('#turn_logical_condition').trigger("change");

				self.logical_conditions = new MultiFormField({
					button_selector: '.add-condition',
					template: jQuery('#dropshipping-logical-conditions > .single-condition:first'),
					wrapper_selector: '#dropshipping-logical-conditions',
					remove_button_selector: '.remove-condition',
					single_item_class: '.single-condition',
					callback: function ( new_item, $wrapper ) {
						jQuery('.conditional-value-type').trigger("change");
						self.droppable.refresh_element();
					}
				});
				self.logical_conditions.init();
				self.init_conditional_values();

				if (jQuery("#dropshipping-logical-conditions .single-condition").length > 0) {
					jQuery("#dropshipping-logical-conditions .single-condition").each(function (index) {
						self.logical_conditions.remove_listener(jQuery(this));
					});
				}

			},
			init_conditional_values: function () { 
				jQuery(document).on('change', '.conditional-value-type', function () {
					let value = jQuery(this).val();
					let row = jQuery(this).closest('.flex-row');
					row.find('.field-wrapper').addClass('hidden');
					row.find('.field-wrapper[data-value="' + value + '"]').removeClass('hidden');
				});
				jQuery('.conditional-value-type').trigger("change");
			},
			init_price_mod_conditions: function () {
				let self = this;

				self.price_mod_conditions = new MultiFormField({
					button_selector: '.add-condition',
					template: jQuery(jQuery('#condition-template').html()),
					wrapper_selector: '.price-mod-conditions',
					remove_button_selector: '.remove-condition',
					single_item_class: '.single-condition',
					before_append_callback: function (new_item, wrapper) {
						let increaseIndex = 0;
						if (wrapper.closest('.price-mod-group').find('.group-counter').length > 0) { 
							increaseIndex = parseInt(wrapper.closest('.price-mod-group').find('.group-counter').html());
						}
						let inputname = new RegExp('\\[' + 0 + '\\]', 'gm');
						let new_item_html = new_item.html().replace(inputname, '[' + increaseIndex + ']');
						return jQuery(new_item_html);
					},
					callback: function ( new_item, $wrapper ) {
						jQuery('.conditional-value-type').trigger("change");
						self.droppable.refresh_element();
					}
				});
				self.price_mod_conditions.init();
				self.init_conditional_values();

				if (jQuery(".price-mod-conditions .single-condition").length > 0) {
					jQuery(".price-mod-conditions .single-condition").each(function (index) {
						self.price_mod_conditions.remove_listener(jQuery(this));
					});
				}

			},
			init_price_groups: function () {
				let self = this;
				
				self.groups = new MultiFormField({
					button_selector: '.add-group',
					template: jQuery(jQuery('#groups-template').html()),
					wrapper_selector: '#price-mod-groups',
					remove_button_selector: '.remove-group',
					single_item_class: '.price-mod-group',
					before_append_callback: function (new_item, wrapper) {
						let increaseIndex = 0;
						if (jQuery('#price-mod-groups').find('.group-counter').last().length > 0) { 
							increaseIndex = parseInt(jQuery('#price-mod-groups').find('.group-counter').last().html());
						}
						increaseIndex += 1;
						new_item.find('.group-counter').html(increaseIndex);
						let inputname = new RegExp('\\[' + 0 + '\\]', 'gm');
						let new_item_html = new_item.html().replace(inputname, '[' + increaseIndex + ']');
						return jQuery(new_item_html);
					},
					callback: function ( new_item, $wrapper ) {
						jQuery('.conditional-value-type').trigger("change");
						self.droppable.refresh_element();
						self.groups.remove_listener(new_item);

					}
				});
				self.groups.init();

				if (jQuery("#price-mod-groups .price-mod-group").length > 0) {
					jQuery("#price-mod-groups .price-mod-group").each(function (index) {
						self.groups.remove_listener(jQuery(this));
					});
				}
			},
			init_product_categories: function () {
				let self = this;
				let $input = jQuery("#category_map_import_id");
				let $form_field = jQuery("#form-field-auto-create-category");

				this.product_categories = new MultiFormField({
					button_selector: '#add_category',
					template: jQuery('#categories-wrapper .single-item:first'),
					wrapper_selector: '#categories-wrapper',
					callback: function () {
						self.droppable.refresh_element();
					}
				});
				self.product_categories.init();
				if (jQuery("#categories-wrapper .single-item").length > 0) {
					jQuery("#categories-wrapper .single-item").each(function (index) {
						self.product_categories.remove_listener(jQuery(this));
					});
				}

				$input.on('change', function (e) {
					self.change_field_state($input.is(':checked'), $form_field, 'form-field-disabled');
				});

				if ($input.is(':checked')) {
					self.change_field_state($input.is(':checked'), $form_field, 'form-field-disabled');
				}
			},
			change_field_state: function (is_active, $form_field, classname) {
				if (is_active) {
					$form_field.addClass(classname);
				} else {
					$form_field.removeClass(classname);
				}
			},
			init_product_tax_classes: function () {
				let self = this;
				this.product_tax_classes = new MultiFormField({
					button_selector: '#add_tax_class',
					template: jQuery('#tax-class-wrapper .single-item:first'),
					wrapper_selector: '#tax-class-wrapper',
					callback: function () {
						self.droppable.refresh_element();
					}
				});
				self.product_tax_classes.init();
				if (jQuery("#tax-class-wrapper .single-item").length > 0) {
					jQuery("#tax-class-wrapper .single-item").each(function (index) {
						self.product_tax_classes.remove_listener(jQuery(this));
					});
				}
			},
			init_downloader: function () {
				this.downloader = new DownloadFile();
				this.downloader.init();
			},
			init_xml_view: function () {
				this.xml_view = new XmlView();
				this.xml_view.refresh(dropshipping_xml_data);
			},
			init_csv_preview: function () {
				let self = this;
				self.preview_csv = new PreviewCsv({
					callback: function (page, content) {
						self.draggable.refresh();
					}
				});
			},
			init_xml_preview: function () {
				let self = this;
				self.preview_xml = new PreviewXml({
					nonce: preview_xml_product_data.nonce,
					action: 'preview_xml_product_data',
					callback: function (page, data) {
						self.xml_view.refresh(data.content);
						if (self.variation_navigator !== null) {
							self.variation_navigator.update_page(1);
						}
						if (self.variation_popup !== null) {
							self.variation_popup.find('.variation-popup-close').trigger('click');
						}
					}
				});
			},
			refresh_variation_xml_preview: function () {
				let self = this;
				let item_nr = jQuery('#dropshipping-variations-item-nr').val();
				let args = {
					'parent_node': jQuery('#dropshipping-node-element').val(),
					'parent_page': jQuery('#dropshipping-item-nr').val()
				};

				if (self.preview_variation_xml == null) {
					self.preview_variation_xml = new PreviewXml({
						action: 'preview_variations_product_data',
						node_element_selector: '#variation_xpath',
						nonce: preview_variations_product_data.nonce,
						loader_selector: '#variation-loader',
						args: args,
						callback: function (page, data) {
							if (self.xml_variation_view == null) {
								self.xml_variation_view = new XmlView({
									xml_view_selector: "#variation-popup-content"
								});
							}
							self.xml_variation_view.refresh(data.content);
							self.variation_navigator.update_items(data.items);
						}
					});
				}
				self.preview_variation_xml.update_args(args);

				if (self.variation_navigator == null) {
					self.variation_navigator = new Navigator({
						page_nr_input_selector: '#dropshipping-variations-item-nr',
						items_nr_input_selector: '#dropshipping-variations-item-position',
						left_button_selector: '#dropshipping-variations-item-position-arrow-left',
						right_button_selector: '#dropshipping-variations-item-position-arrow-right',
						callback: function (page) {
							self.preview_variation_xml.change_page(page);
						}
					});
					self.variation_navigator.init();
					self.variation_navigator.update_page(1);
					self.preview_variation_xml.change_page(1);
				}


				self.variation_navigator.update_page(item_nr);
				self.preview_variation_xml.change_page(item_nr);
			},
			init_navigator: function () {
				let self = this;
				self.navigator = new Navigator({
					callback: function (page) {
						let preview = jQuery('#dropshipping-csv-table').length ? self.preview_csv : self.preview_xml;
						if (preview !== null) {
							preview.change_page(page);
						}
					}
				});
				self.navigator.init();
			},
			init_csv_converter: function () {
				let self = this;
				self.csv_converter = new ConvertCsv({
					callback: function (items) {
						self.navigator.update_items(items);
						self.navigator.update_page(1);
						self.preview_csv.change_page(1);
					}
				});
				self.csv_converter.init();
			},
			init_xml_converter: function () {
				let self = this;
				self.xml_converter = new ConvertXml({
					callback: function (items, element_id, content) {
						self.navigator.update_items(items);
						self.navigator.update_page(1);
						self.preview_xml.update_element(element_id);
						self.xml_view.refresh(content);
					}
				});
				self.xml_converter.init();
			},
			init_import: function () {
				this.import = new Import({
					'callback': function () {
						jQuery('#navigation-wrapper').removeClass('hidden');
					}
				});
				this.import.init();
				this.import.autostart(true);
			},
			init_select2_fields: function () {
				jQuery('.dropshipping-select2').select2();
				jQuery('.dropshipping-select2.width-100').select2({ width: '100%' });
			},
			init_next_page_jump_preventer: function () {
				jQuery(window).keydown(function (e) {
					if (e.which === 13 && !jQuery(e.target).is('textarea')) {
						e.preventDefault();
						return false;
					}
				});
			},
			init_mapper_selector: function () {
				jQuery('.mapper-has-parent-selector').each(function () {
					if (jQuery(this).is(':checked')) {
						jQuery(this).closest('.mapper-selector-container').addClass('has_parent_image');
					}
				});
				jQuery('.mapper-has-parent-selector').on('click', function (e) {
					jQuery(this).closest('.mapper-selector-container').toggleClass('has_parent_image');
				});
				jQuery('.mapper-type-selector').each(function () {
					if (jQuery(this).is(':checked')) {
						jQuery(this).closest('.mapper-selector-container').addClass('active');
					}
				});
				jQuery('.mapper-type-selector').on('click', function (e) {
					jQuery('.mapper-selector-container').removeClass('active');
					jQuery(this).closest('.mapper-selector-container').addClass('active');
				});
			},
			init_fixed_sidebar: function () {
				this.fixed_sidebar = new FixedSidebar();
				this.fixed_sidebar.init();
			},
			init_options_settings: function () {
				let self = this;

				if (this.has_logical_condition()) {
					this.init_logical_conditions();
				}
				
				if (this.has_price_groups()) {
					this.init_price_groups();
					this.init_price_mod_conditions();
				}

				let $input = jQuery("#update_only_existing");
				let $form_field = jQuery("#create_new_products_as_draft").closest('tr');
				$input.on('change', function (e) {
					self.change_field_state($input.is(':checked'), $form_field, 'hidden');
				});

				if ($input.is(':checked')) {
					self.change_field_state($input.is(':checked'), $form_field, 'hidden');
				}
			},
			init_mapper_settings: function () {
				let self = this;

				let $input = jQuery("#variation_type_embedded");
				let $form_field = jQuery(".options_group.pricing");
				let $product_type = jQuery("#product-type");
				jQuery('.mapper-type-selector').on('change', function (e) {
					self.change_field_state($input.is(':checked'), $form_field, 'hidden-pricing');
				});

				$product_type.on('change', function (e) {
					self.change_field_state( $input.is(':checked') && $product_type.val() === 'variable', $form_field, 'hidden-pricing');
				});

				self.change_field_state($input.is(':checked') && $product_type.val() === 'variable', $form_field, 'hidden-pricing');
			},
			init_variation_popup: function () {
				let self = this;
				self.variation_popup = jQuery('#variation-popup');

				self.variation_popup.draggable();

				jQuery('#open-variation-window').on('click', function (e) {
					e.preventDefault();
					self.variation_popup.addClass('open');
					self.refresh_variation_xml_preview();
				});

				self.variation_popup.find('.variation-popup-close').on('click', function (e) {
					e.preventDefault();
					self.variation_popup.removeClass('open');
				});

			},
			init_variation_fields: function () {
				let self = this;
				let variation_xpath = jQuery('#variation_xpath');
				let variation_parent_wrapper = jQuery('#variation_parent_options_wrapper');
				let variation_parent_selector = jQuery('#variation_parent_selector');
				let variation_parent_options = jQuery('#variation_parent_options');
				let variation_stock_manage = jQuery('#variation_manage_stock');
				let variation_tax_class_manage = jQuery('#variation_tax_class_xpath_switcher');


				if (variation_xpath.length > 0) {
					self.change_variation_fields(variation_xpath);

					variation_xpath.on('drop change', function (e) {
						e.preventDefault();
						self.change_variation_fields(variation_xpath);
					});
				}

				if (variation_parent_selector.length > 0) {
					if (variation_parent_selector.is(':checked')) {
						variation_parent_wrapper.removeClass('hidden');
						self.change_parent_fields_state(variation_parent_options);
					}
				}

				variation_parent_selector.on('change', function () {
					if (jQuery(this).is(':checked')) {
						variation_parent_wrapper.removeClass('hidden');
						self.change_parent_fields_state(variation_parent_options);
					} else {
						variation_parent_wrapper.addClass('hidden');
						jQuery('#variation-hidden-fields').find('.form-field-disabled').removeClass('form-field-disabled');
					}
				});

				variation_parent_options.on('change', function () {
					self.change_parent_fields_state(variation_parent_options);
				});

				variation_stock_manage.on('change', function () {
					self.change_stock_manage_state(jQuery(this).is(':checked'));
				});
				variation_stock_manage.trigger('change');

				variation_tax_class_manage.on('change', function () {
					self.change_tax_class_manage_state(jQuery(this).is(':checked'));
				});
				variation_tax_class_manage.trigger('change');

				self.variation_product_tax_classes = new MultiFormField({
					button_selector: '#add_variation_tax_class',
					template: jQuery('#variation-tax-class-wrapper .single-item:first'),
					wrapper_selector: '#variation-tax-class-wrapper',
					callback: function () {
						self.droppable.refresh_element();
					}
				});
				self.variation_product_tax_classes.init();
				if (jQuery("#variation-tax-class-wrapper .single-item").length > 0) {
					jQuery("#variation-tax-class-wrapper .single-item").each(function (index) {
						self.variation_product_tax_classes.remove_listener(jQuery(this));
					});
				}
			},

			change_stock_manage_state: function (checked) {
				let $static = jQuery('[data-variation-stock="static"]');
				let $dynamic = jQuery('[data-variation-stock="dynamic"]');

				if (checked === true) {
					$static.addClass('hidden');
					$dynamic.removeClass('hidden');
				} else {
					$dynamic.addClass('hidden');
					$static.removeClass('hidden');
				}
			},

			change_tax_class_manage_state: function (checked) {
				let $xpath = jQuery('[data-variation-tax-class="xpath"]');

				if (checked === true) {
					$xpath.removeClass('hidden');
				} else {
					$xpath.addClass('hidden');
				}
			},

			change_parent_fields_state: function (selector) {
				jQuery.each(selector.find('option'), function (index, value) {
					jQuery('[data-variation-field="' + jQuery(this).attr('value') + '"]').removeClass('form-field-disabled');
				});


				jQuery.each(selector.val(), function (index, value) {
					jQuery('[data-variation-field="' + value + '"]').addClass('form-field-disabled');
				});
			},

			change_variation_fields: function (input, show_popup = false) {
				let regex = new RegExp('\{(.*?)\}');
				let hidden_fields = jQuery('#variation-hidden-fields');

				setTimeout(function () {
					if (regex.test(input.val())) {
						input.val(input.val().replace(/\[.*?\]/g, ''));
						hidden_fields.removeClass('hidden');
					} else {
						hidden_fields.addClass('hidden');
					}

				}, 500);
			},
			init_group_switcher: function () {
				jQuery(document).on('click', '.group-switcher', function (e) {
					e.preventDefault();
					jQuery(this).closest('.group-switch').toggleClass('group-closed');
				});
			},
			init: function () {
				this.init_select2_fields();
				this.init_next_page_jump_preventer();

				this.init_droppable();
				this.init_draggable();
				this.init_product_attributes();
				this.init_variation_attributes();
				this.init_product_categories();
				this.init_product_tax_classes();
				this.init_variation_fields();
				this.init_variation_popup();
				this.init_downloader();

				if (this.is_xml_page()) {
					this.init_xml_view();
					this.init_xml_preview();
					this.init_navigator();
					this.init_xml_converter();
				} else {
					this.init_csv_preview();
					this.init_navigator();
					this.init_csv_converter();
				}

				if (this.is_import_page()) {
					this.init_import();
				}

				if (this.has_sidebar()) {
					this.init_fixed_sidebar();
				}

				if (this.has_mapper_selector()) {
					this.init_mapper_selector();
				}

				this.init_options_settings();
				this.init_mapper_settings();
				this.init_group_switcher();
			},
			/**
			 * @returns {bool}.
			 */
			is_xml_page: function () {
				return jQuery('#xml-view').length > 0;
			},

			/**
			 * @returns {bool}.
			 */
			is_import_page: function () {
				return jQuery('#log-viewer').length > 0;
			},

			has_logical_condition: function () {
				return jQuery('#dropshipping-logical-conditions').length > 0;
			},
			has_price_groups: function () {
				return jQuery('#price-mod-groups').length > 0;
			},
			/**
			 * @returns {bool.
			 */
			has_sidebar: function () {
				return jQuery('#dropshipping-sidebar-content').length > 0;
			},

			has_mapper_selector: function () {
				return jQuery('.mapper-type-selector').length > 0;
			}
		}

		DropshippingAdmin.init();
	});
})(jQuery);
