"use strict";

/**
 * Navigator.
 *
 * @param {Object} options.
 * @constructor
 */
export default function Navigator(options) {

	let settings = {
		page_nr_input_selector: '#dropshipping-item-nr',
		items_nr_input_selector: '#dropshipping-item-position',
		left_button_selector: '#dropshipping-item-position-arrow-left',
		right_button_selector: '#dropshipping-item-position-arrow-right',
		callback: null
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.page_input = jQuery(settings.page_nr_input_selector);
	this.items = jQuery(settings.items_nr_input_selector);
	this.left_button = jQuery(settings.left_button_selector);
	this.right_button = jQuery(settings.right_button_selector);
	this.callback = settings.callback;
}

Navigator.prototype = {
	page_input: null,
	items: null,
	left_button: null,
	right_button: null,
	callback: null,

	/**
	 * @param {integer} page.
	 * @param {mixed} callback, callable function or null.
	 */
	change_page: function (page, callback) {
		const self = this;
		this.page_input.val(page);
		if (typeof self.callback === "function") {
			self.callback(page);
		}
	},
	add_listeners: function () {
		const self = this;
		self.left_button.on('click', function (e) {
			e.preventDefault();
			if (self.page_input.val() > 1) {
				let page = self.page_input.val() - 1;
				self.change_page(page, self.callback);
			}
		});
		self.right_button.on('click', function (e) {
			e.preventDefault();
			if (parseInt(self.items.html()) > self.page_input.val()) {
				let page = parseInt(self.page_input.val()) + 1;
				self.change_page(page, self.callback);
			}
		});
		self.page_input.on('change', function () {
			self.change_page(self.page_input.val(), self.callback);
		});
	},
	/**
	 * @param {string} items.
	 */
	update_items: function (items) {
		this.items.html(items);
	},
	/**
	 * @param {integer} number.
	 */
	update_page: function (number) {
		this.page_input.val(number);
	},
	init: function () {
		this.add_listeners();
	}
}
