"use strict";

/**
 * Xml View.
 *
 * @param {Object} options.
 * @constructor
 */
export default function XmlView(options) {

	let settings = {
		xml_view_selector: "#xml-view",
		draggable: new DraggableElements()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.xml_view = jQuery(settings.xml_view_selector);
	this.draggable = settings.draggable;
}

XmlView.prototype = {
	xml_view: null,
	draggable: null,

	/**
	 * @param {string} xml.
	 */
	refresh: function (xml) {
		const self = this;
		try {
			self.xml_view.empty().simpleXML({
				xmlString: xml.trim(),
				callback: function () {
					self.draggable.refresh();
				}
			});
		} catch (e) {
			self.xml_view.empty().append("Exception caught running plugin : " + e);
			console.log( e );
			alert(e);
		}
	},

	/**
	 * @returns {bool}.
	 */
	view_exists: function () {
		return this.xml_view.length > 0;
	}
}
