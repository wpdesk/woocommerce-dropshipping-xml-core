"use strict";

/**
 * Preview CSV.
 *
 * @param {Object} options.
 * @constructor
 */
export default function PreviewCsv(options) {

	let settings = {
		table_wrapper_selector: '#dropshipping-csv-table',
		loader_selector: '#dropshipping-table-loader',
		notice: new ImportNotice(),
		callback: null,
		request: new BrowserRequest(),
		exception: new Exception()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.table = jQuery(settings.table_wrapper_selector);
	this.loader = jQuery(settings.loader_selector);
	this.notice = settings.notice;
	this.callback = settings.callback;
	this.request = settings.request;
	this.exception = settings.exception;
}

PreviewCsv.prototype = {
	table: null,
	loader: null,
	notice: null,
	callback: null,
	exception: null,
	/**
	 * @param {integer} page_nr.
	 */
	preview: function (page_nr) {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'preview_csv_product_data',
				item_number: page_nr,
				uid: self.request.getUid(),
				security: preview_csv_product_data.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.notice.success_notice(data.message);
					self.table.html(data.content);
					if (typeof self.callback === "function") {
						self.callback(page_nr, data.content);
					}
					self.loader.hide();
				} else {
					self.notice.error_notice(data.message);
					self.loader.hide();
				}
			},
			error: function ( jqXHR, exception ) {
				self.notice.error_notice(self.exception.get_message( jqXHR, exception ));
				self.loader.hide();
			}
		});
	},

	/**
	 * @param {integer} value.
	 */
	change_page: function (page) {
		this.loader.show();
		this.preview(page);
	}
}
