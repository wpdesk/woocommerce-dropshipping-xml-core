"use strict";

/**
 * Preview XML.
 *
 * @param {Object} options.
 * @constructor
 */
export default function PreviewXml(options) {

	let settings = {
		node_element_selector: '#dropshipping-node-element',
		loader_selector: '#dropshipping-table-loader',
		notice: new ImportNotice(),
		request: new BrowserRequest(),
		exception: new Exception(),
		action: '',
		nonce: '',
		args: {}
	}
	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.element = jQuery(settings.node_element_selector);
	this.loader = jQuery(settings.loader_selector);
	this.notice = settings.notice;
	this.callback = settings.callback;
	this.request = settings.request;
	this.exception = settings.exception;
	this.action = settings.action;
	this.nonce = settings.nonce;
	this.args = settings.args;
}

PreviewXml.prototype = {
	element: null,
	action: null,
	nonce: null,
	loader: null,
	notice: null,
	callback: null,
	request: null,
	exception: null,
	args: null,
	/**
	 * @param {integer} page_nr.
	 */
	preview: function (page_nr) {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: self.action,
				item_number: page_nr,
				node_element: self.element.val(),
				uid: self.request.getUid(),
				args: self.args,
				security: self.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.notice.success_notice(data.message);
					if (typeof self.callback === "function") {
						self.callback(page_nr, data);
					}
					self.loader.hide();
				} else {
					self.notice.error_notice(data.message);
					self.loader.hide();
				}
			},
			error: function ( jqXHR, exception ) {
				self.notice.error_notice(self.exception.get_message( jqXHR, exception ));
				self.loader.hide();
			}
		});
	},

	/**
	 * @param {integer} value.
	 */
	update_element: function (value) {
		this.element.val(value);
	},

	/**
	 * @param {integer} value.
	 */
	change_page: function (page) {
		this.loader.show();
		this.preview(page);
	},

	/**
	 * @param {object} value.
	 */
	update_args: function (args) {
		this.args = args;
	}
}
