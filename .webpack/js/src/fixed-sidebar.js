"use strict";

/**
 * Import.
 *
 * @param {Object} options.
 * @constructor
 */
export default function FixedSidebar(options) {

	let settings = {
		admin_bar_selector: '#wpadminbar',
		sidebar_selector: '#postbox-container-1',
		sidebar_content_selector: '#dropshipping-sidebar-content'
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.admin_bar = jQuery(settings.admin_bar_selector);
	this.sidebar = jQuery(settings.sidebar_selector);
	this.sidebar_content = jQuery(settings.sidebar_content_selector);
}

FixedSidebar.prototype = {
	admin_bar: null,
	sidebar: null,
	sidebar_content: null,
	top_offset: null,
	scroll: function(){
		let self = this;
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > (self.top_offset)) {
				if (!self.sidebar.hasClass('fixed')) {
					self.sidebar.addClass('fixed');
					self.sidebar.css('top', self.admin_bar.height() + 'px')
				}
			} else {
				if (self.sidebar.hasClass('fixed')) {
					self.sidebar.removeClass('fixed');
					self.sidebar.css('top', 'auto')
				}
			}
		});
	},
	init: function(){
		this.top_offset = this.sidebar.offset().top - this.admin_bar.height();
		let tableContentHeight = jQuery(window).height() - (this.sidebar_content.offset().top - this.sidebar.offset().top) - this.admin_bar.height() - 70;
		this.sidebar_content.css('max-height', tableContentHeight + 'px');
		this.scroll();
	}
}
