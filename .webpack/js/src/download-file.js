"use strict";

/**
 * Download file.
 *
 * @param {Object} options.
 * @constructor
 */
export default function DownloadFile(options) {

	let settings = {
		button_selector: '#import',
		next_step_selector: '#next_step',
		original_file_format_selector: '#original_file_format',
		original_file_name_selector: '#original_file_name',
		loader_selector: '#droppshiping-import-loader',
		notice: new ImportNotice(),
		exception: new Exception()
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.import_button = jQuery(settings.button_selector);
	this.next_step_button = jQuery(settings.next_step_selector);
	this.loader = jQuery(settings.loader_selector);
	this.original_file_format = jQuery(settings.original_file_format_selector);
	this.original_file_name = jQuery(settings.original_file_name_selector);
	this.notice = settings.notice;
	this.exception = settings.exception;
}

DownloadFile.prototype = {
	import_button: null,
	next_step_button: null,
	loader: null,
	original_file_format: null,
	original_file_name: null,
	notice: null,
	exception: null,

	/**
	 * @param {string} form, serialized form data.
	 */
	download: function (form) {
		const self = this;
		jQuery.ajax({
			type: "POST",
			cache: false,
			url: ajaxurl,
			data: {
				action: 'download_product_data',
				data: form,
				security: download_product_data.nonce
			},
			success: function (data) {
				if (data.success === true) {
					self.notice.success_notice(data.message);
					self.import_button.attr('disabled', false);
					self.next_step_button.attr('disabled', false);
					self.original_file_format.val(data.original_file_format);
					self.original_file_name.val(data.original_file_name);
					self.loader.hide();
				} else {
					self.import_button.attr('disabled', false);
					self.notice.error_notice(data.message);
					self.loader.hide();
				}
			},
			error: function (jqXHR, exception) {
				self.import_button.attr('disabled', false);
				self.notice.error_notice(self.exception.get_message(jqXHR, exception));
				self.loader.hide();
			}
		});
	},
	add_listeners: function () {
		const self = this;
		self.import_button.on('click', function (e) {
			e.preventDefault();
			self.import_button.attr('disabled', true);
			self.next_step_button.attr('disabled', true);
			self.loader.show();
			self.notice.hide_notice();
			self.download(jQuery(this).closest('form').serialize());
		})
	},
	init: function () {
		this.add_listeners();
	}
}
