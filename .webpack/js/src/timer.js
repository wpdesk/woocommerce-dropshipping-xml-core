"use strict";

/**
 * Timer.
 *
 * @param {Object} options.
 * @constructor
 */
export default function Timer(options) {

	let settings = {
		node_element_selector: '#timer',
	}

	if (typeof options == "object") {
		jQuery.extend(settings, options);
	}

	this.element = jQuery(settings.node_element_selector);
}

Timer.prototype = {
	element: null,
	interval: null,
	elapsed_seconds: 0,
	hours: 0,
	minutes: 0,
	seconds: 0,
	update_timer: function () {
		this.elapsed_seconds = this.elapsed_seconds + 1;
	},

	/**
	 * @param {string} time
	 */
	update_element: function (time) {
		this.element.text(time);
	},

	/**
	 * @returns {string}.
	 */
	get_formated_time: function () {
		let total_seconds = this.elapsed_seconds;

		let hours = Math.floor(total_seconds / 3600);
		total_seconds = this.elapsed_seconds % 3600;

		let minutes = Math.floor(total_seconds / 60);
		total_seconds = this.elapsed_seconds % 60;

		let seconds = Math.floor(total_seconds);

		this.hours = this.get_pretty_time(hours);
		this.minutes = this.get_pretty_time(minutes);
		this.seconds = this.get_pretty_time(seconds);

		return this.hours + ":" + this.minutes + ":" + this.seconds;
	},

	/**
	 * @param {integer} num.
	 * @returns {string}.
	 */
	get_pretty_time: function (num) {
		return (num < 10 ? "0" : "") + num;
	},
	start_timer: function () {
		const self = this;
		self.interval = setInterval(function () {
			self.update_timer();
			self.update_element(self.get_formated_time());
		}, 1000);
	},
	stop_timer: function () {
		clearInterval(this.interval);
	}
}
