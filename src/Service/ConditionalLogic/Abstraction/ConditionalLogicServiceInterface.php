<?php

namespace WPDesk\Library\DropshippingXmlCore\Service\ConditionalLogic\Abstraction;

/**
 * intreface ConditionalLogic
 *
 * @package WPDesk\Library\DropshippingXmlCore\ConditionalLogic
 */
interface ConditionalLogicServiceInterface {


	public function is_valid(): bool;
}
