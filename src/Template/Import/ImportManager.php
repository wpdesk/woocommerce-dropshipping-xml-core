<?php

use WPDesk\Library\DropshippingXmlCore\Action\View\ImportMapperViewAction;
use WPDesk\Library\DropshippingXmlCore\Action\View\ImportOptionsViewAction;
use WPDesk\Library\DropshippingXmlCore\Action\View\ImportStatusViewAction;
use WPDesk\Library\DropshippingXmlCore\Action\View\ImportManagerViewAction;
use WPDesk\Library\DropshippingXmlCore\Form\ImportManagerForm;
use WPDesk\Library\DropshippingXmlCore\Action\View\ImportFileViewAction;

/**
 * @var string $title
 * @var string $import_url
 * @var string $header_url
 * @var WPDesk\View\Renderer\Renderer $renderer
 * @var \WPDesk\Library\DropshippingXmlCore\Infrastructure\View\FormView $form
 * @var \WPDesk\Library\DropshippingXmlCore\Helper\PluginHelper $plugin_helper
 * @var \WPDesk\Library\DropshippingXmlCore\Entity\Import[] $imports
 * @var string $nonce
 * @var string $elements number of all import items
 */

$display_import_status = function ( \WPDesk\Library\DropshippingXmlCore\Entity\Import $import ) {
	$status = $class = $error_message = ''; // phpcs:ignore

	switch ( $import->get_status() ) {
		case \WPDesk\Library\DropshippingXmlCore\Entity\Import::STATUS_WAITING:
			$class  = 'color-success';
			$status = __( 'Synchronization active', 'woocommerce-dropshipping-xml-core' );
			break;
		case \WPDesk\Library\DropshippingXmlCore\Entity\Import::STATUS_STOPPED:
			$class  = 'color-error';
			$status = __( 'Synchronization stopped', 'woocommerce-dropshipping-xml-core' );
			break;
		case \WPDesk\Library\DropshippingXmlCore\Entity\Import::STATUS_ERROR:
			$class         = 'color-error';
			$status        = __( 'Synchronization error', 'woocommerce-dropshipping-xml-core' );
			$error_message = '<p class="' . $class . '">' . $import->get_error_message() . '</p>';
			break;
		default:
			$class  = 'color-success';
			$status = __( 'Synchronization in progress', 'woocommerce-dropshipping-xml-core' );
			break;
	}

	return str_replace( [ '{class}', '{status}', '{error}' ], [ $class, $status, $error_message ], '<span class="{class}"><strong>{status}</strong>{error}</span>' );
};

$display_action_url = function ( \WPDesk\Library\DropshippingXmlCore\Entity\Import $import ) use ( $plugin_helper, $nonce ) {
	switch ( $import->get_status() ) {
		case \WPDesk\Library\DropshippingXmlCore\Entity\Import::STATUS_STOPPED:
		case \WPDesk\Library\DropshippingXmlCore\Entity\Import::STATUS_ERROR:
			$anchor = __( 'Activate synchronization', 'woocommerce-dropshipping-xml-core' );

			$url = $plugin_helper->generate_url_by_view(
				ImportManagerViewAction::class,
				[
					'activate' => $import->get_id(),
					'nonce'    => $nonce,
				]
			);
			break;
		default:
			$anchor = __( 'Stop synchronization', 'woocommerce-dropshipping-xml-core' );
			$url    = $plugin_helper->generate_url_by_view(
				ImportManagerViewAction::class,
				[
					'stop'  => $import->get_id(),
					'nonce' => $nonce,
				]
			);
			break;
	}

	return str_replace( [ '{url}', '{anchor}' ], [ $url, $anchor ], '	<a href="{url}">{anchor}</a>' );
};

	$renderer->output_render(
		'Header',
		[
			'title'      => $title,
			'header_url' => $header_url,
		]
	);
	?>

<p style="font-weight:bold;"><?php echo wp_kses_post( __( 'Read more in the <a href="https://wpde.sk/dropshipping-import-2" class="docs-url" target="_blank">plugin documentation &rarr;</a>', 'woocommerce-dropshipping-xml-core' ) ); ?></p>
<p><?php echo wp_kses_post( __( 'Have you encountered any problems with the import or want to know more? <a href="https://wpde.sk/dropshipping-faq" class="docs-url" target="_blank">Visit FAQ  &rarr;</a>.', 'woocommerce-dropshipping-xml-core' ) ); ?></p>


<?php $form->form_start(); ?>
<div class="tablenav top">
	<div class="alignleft actions bulkactions">
		<?php $form->show_field( 'manage' ); ?>
		<?php $form->show_field( 'apply' ); ?>
	</div>
	<div class="tablenav-pages one-page">
		<span class="displaying-num"><?php echo esc_html( $elements ); ?></span>
		<br class="clear">
	</div>
	<table class="wp-list-table widefat fixed striped posts">
		<thead>
			<tr>
				<td id="cb" class="manage-column column-cb check-column">
					<label class="screen-reader-text" for="cb-select-all-1"><?php echo esc_html( __( 'Select all', 'woocommerce-dropshipping-xml-core' ) ); ?></label>
					<input id="cb-select-all-1" type="checkbox">
				</td>
				<th scope="col" id="id" class="manage-column column-id desc">
					<span><?php echo esc_html( __( 'ID', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="schedule" class="manage-column column-schedule">
					<span><?php echo esc_html( __( 'Cron schedule', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="actions" class="manage-column column-actions">
					<span><?php echo esc_html( __( 'Actions', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="import-status" class="manage-column column-import-status">
					<span><?php echo esc_html( __( 'Status', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="import-stats" class="manage-column column-import-status">
					<span><?php echo esc_html( __( 'Import statistics', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="date" class="manage-column column-date asc">
					<span><?php echo esc_html( __( 'Created', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
			</tr>
		</thead>

		<tbody id="the-list">
			<?php if ( empty( $imports ) ) : ?>
				<tr>
					<td colspan="5">
						<span>
						<?php
						/* TRANSLATORS: %s: http url to import page */
						echo wp_kses_post( sprintf( __( 'Import list is empty, <a href="%s" target="_self">please create your first import.</a>', 'woocommerce-dropshipping-xml-core' ), esc_url( $import_url ) ) );
						?>
						</span>
					</td>
				</tr>
			<?php else : ?>

				<?php foreach ( $imports as $import_file ) : ?>
					<tr id="post-<?php echo esc_html( $import_file->get_id() ); ?>" class="iedit level-0 status-publish hentry">
						<th scope="row" class="check-column">
							<input id="cb-select-<?php echo esc_html( $import_file->get_id() ); ?>" type="checkbox" name="<?php echo esc_html( ImportManagerForm::ID ); ?>[id][]" value="<?php echo esc_html( $import_file->get_id() ); ?>">
						</th>
						<td class="id column-id has-row-actions column-primary">
							<?php
							if ( ! empty( $import_file->get_import_name() ) ) {
								echo '<strong><span class="row-file-name">' . $import_file->get_import_name() . '</span></strong><br>';
								echo '<small><span class="row-file">' . esc_url( $import_file->get_url() ) . '</span></small>';
							} else {
								echo '<strong><span class="row-file">' . esc_url( $import_file->get_url() ) . '</span></strong>';
							}
							?>
							<div class="row-actions">
							<span class="edit">
									<a href="
									<?php
									echo esc_url(
										$plugin_helper->generate_url_by_view(
											ImportFileViewAction::class,
											[
												'uid'  => $import_file->get_uid(),
												'mode' => 'edit_all',
											]
										)
									);
									?>
									"><?php echo esc_html( __( 'Edit', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="edit">
									<a href="
									<?php
									echo esc_url(
										$plugin_helper->generate_url_by_view(
											ImportMapperViewAction::class,
											[
												'uid'  => $import_file->get_uid(),
												'mode' => 'edit',
											]
										)
									);
									?>
												"><?php echo esc_html( __( 'Edit mapper', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="edit">
									<a href="
									<?php
									echo esc_url(
										$plugin_helper->generate_url_by_view(
											ImportOptionsViewAction::class,
											[
												'uid'  => $import_file->get_uid(),
												'mode' => 'edit',
											]
										)
									);
									?>
												"><?php echo esc_html( __( 'Edit options', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="edit">
									<a href="<?php echo esc_url( $plugin_helper->get_url_to_import_file( $import_file->get_uid() ) ); ?>"><?php echo esc_html( __( 'Imported file preview', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="edit">
									<a href="<?php echo esc_url( $plugin_helper->get_url_to_log_file( $import_file->get_uid() ) ); ?>"><?php echo esc_html( __( 'Log', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="edit">
									<a href="
									<?php
									echo esc_url(
										$plugin_helper->generate_url_by_view(
											ImportManagerViewAction::class,
											[
												'clone'  => $import_file->get_uid(),
												'nonce'  => $nonce,
											]
										)
									);
									?>
												"><?php echo esc_html( __( 'Clone', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
								<span class="trash">
									<a href="
									<?php
									echo esc_url(
										$plugin_helper->generate_url_by_view(
											ImportManagerViewAction::class,
											[
												'delete' => $import_file->get_id(),
												'nonce'  => $nonce,
											]
										)
									);
									?>
												"><?php echo esc_html( __( 'Delete', 'woocommerce-dropshipping-xml-core' ) ); ?></a> |
								</span>
							</div>
						</td>
						<td class="id column-id">
							<span title="Cron Schedule"><?php echo esc_html( $import_file->get_cron_schedule() ); ?></span>
						</td>
						<td class="id column-id">
							<ul>
								<li>
									<strong>
										<a href="<?php echo esc_url( $plugin_helper->generate_url_by_view( ImportStatusViewAction::class, [ 'uid' => $import_file->get_uid() ] ) ); ?>"><?php echo esc_html( __( 'Import now', 'woocommerce-dropshipping-xml-core' ) ); ?></a>
									</strong>
								</li>
								<li>
									<strong>
										<?php echo wp_kses_post( $display_action_url( $import_file ) ); ?>
									</strong>
								</li>
							</ul>
						</td>
						<td class="id column-id">
							<ul>
								<li>
									<?php echo wp_kses_post( $display_import_status( $import_file ) ); ?>
								</li>
							</ul>
						</td>
						<td class="id column-id">
							<ul>
								<li>
									<?php echo esc_html( __( 'Created', 'woocommerce-dropshipping-xml-core' ) ); ?>: <strong><?php echo esc_html( $import_file->get_created() ); ?></strong>
								</li>
								<li>
									<?php echo esc_html( __( 'Updated', 'woocommerce-dropshipping-xml-core' ) ); ?>: <strong><?php echo esc_html( $import_file->get_updated() ); ?></strong>
								</li>
								<li>
									<?php echo esc_html( __( 'Skipped', 'woocommerce-dropshipping-xml-core' ) ); ?>: <strong><?php echo esc_html( $import_file->get_skipped() ); ?></strong>
								</li>
							</ul>
						</td>
						<td class="date column-date"><span title="<?php echo esc_html( $import_file->get_date_created() ); ?>"><?php echo esc_html( $import_file->get_date_created() ); ?></span></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>

		</tbody>
		<tfoot>
			<tr>
				<td id="cb2" class="manage-column column-cb check-column">
					<label class="screen-reader-text" for="cb-select-all-1"><?php echo esc_html( __( 'Select all', 'woocommerce-dropshipping-xml-core' ) ); ?></label>
					<input id="cb-select-all-1" type="checkbox">
				</td>
				<th scope="col" id="id2" class="manage-column column-id desc">
					<span><?php echo esc_html( __( 'ID', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="schedule2" class="manage-column column-schedule">
					<span><?php echo esc_html( __( 'Cron schedule', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="actions2" class="manage-column column-actions">
					<span><?php echo esc_html( __( 'Actions', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="import-status2" class="manage-column column-import-status">
					<span><?php echo esc_html( __( 'Status', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="import-stats2" class="manage-column column-import-status">
					<span><?php echo esc_html( __( 'Import statistics', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
				<th scope="col" id="date2" class="manage-column column-date asc">
					<span><?php echo esc_html( __( 'Created', 'woocommerce-dropshipping-xml-core' ) ); ?></span>
				</th>
			</tr>
		</tfoot>
	</table>

	<div class="tablenav bottom">
		<div class="alignleft actions bulkactions">
			<?php $form->show_field( 'manage2' ); ?>
			<?php $form->show_field( 'apply2' ); ?>
		</div>
		<div class="alignleft actions"></div>
		<div class="tablenav-pages one-page"><span class="displaying-num"><?php echo esc_html( $elements ); ?></span><br class="clear"></div>
	</div>
	<?php
	$form->form_fields_complete();
	$form->form_end();
	$renderer->output_render( 'Footer' );
