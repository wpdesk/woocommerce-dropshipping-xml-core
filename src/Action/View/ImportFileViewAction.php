<?php
namespace WPDesk\Library\DropshippingXmlCore\Action\View;

use WPDesk\View\Renderer\Renderer;
use WPDesk\Library\DropshippingXmlCore\Form\ImportFileForm;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Service\Listener\Items\Registrable\Registrable;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\View\Abstraction\Displayable;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\View\FormView;
use WPDesk\Library\DropshippingXmlCore\Action\Process\Form\ImportFileFormProcessAction;
use WPDesk\Library\DropshippingXmlCore\DataProvider\ImportFileDataProvider;
use WPDesk\Library\DropshippingXmlCore\View\ImportFileView;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Service\Listener\Items\Initable\Initable;
use WPDesk\Library\DropshippingXmlCore\Factory\DataProviderFactory;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Request\Request;


/**
 * Class ImportFileViewAction, import file view action.
 *
 * @package WPDesk\Library\DropshippingXmlCore\Action\View
 */
class ImportFileViewAction implements Displayable, Registrable, Initable {

	/**
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @var ImportFileForm
	 */
	private $form;

	/**
	 * @var DataProviderFactory
	 */
	private $data_provider_factory;

	/**
	 * @var Request
	 */
	private $request;

	public function __construct(
		Request $request,
		Renderer $renderer,
		ImportFileForm $form,
		DataProviderFactory $data_provider_factory
	) {
		$this->request               = $request;
		$this->renderer              = $renderer;
		$this->form                  = $form;
		$this->data_provider_factory = $data_provider_factory;
	}

	public function register(): array {
		return [
			ImportFileFormProcessAction::class,
		];
	}

	public function init() {
		$uid = $this->request->get_param( 'get.uid' )->getAsString();
		if ( ! empty( $uid ) ) {
			$data_provider = $this->data_provider_factory->create_by_class_name( ImportFileDataProvider::class, [ 'postfix' => $uid ] );
			$this->form->set_data( $data_provider );
		}
	}

	public function show() {

		$mode = $this->request->get_param( 'get.mode' )->getAsString();

		$data = [
			'title'         => __( 'File import', 'woocommerce-dropshipping-xml-core' ),
			'mode'          => $mode,
			'edit'          => false,
			'has_ftp_addon' => $this->has_ftp_addon(),
			'form'          => new FormView( $this->form, $this->renderer ),
			'renderer'      => $this->renderer,
		];

		$this->renderer->output_render( 'Import/ImportConnector', $data );
	}

	private function has_ftp_addon() {
		return is_plugin_active( 'advanced-import-for-dropshipping/advanced-import-for-dropshipping.php' );
	}
}
