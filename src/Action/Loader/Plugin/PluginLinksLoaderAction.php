<?php
namespace WPDesk\Library\DropshippingXmlCore\Action\Loader\Plugin;

use WPDesk\Library\DropshippingXmlCore\Helper\PluginHelper;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Service\Listener\Items\Hookable\Hookable;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Config\Config;
use WPDesk\Library\DropshippingXmlCore\Infrastructure\Request\Request;
use WPDesk\Library\DropshippingXmlCore\Action\View\ImportManagerViewAction;
use WPDesk\Library\DropshippingXmlCore\Action\View\MarketingViewAction;


/**
 * Class PluginLinksLoaderAction, plugin links loader.
 */
class PluginLinksLoaderAction implements Hookable {

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var PluginHelper
	 */
	private $plugin_helper;

	public function __construct( Config $config, Request $request, PluginHelper $helper ) {
		$this->config        = $config;
		$this->request       = $request;
		$this->plugin_helper = $helper;
	}

	public function hooks() {
		add_filter( 'plugin_action_links_' . \plugin_basename( $this->config->get_param( 'plugin.file' )->get() ), [ $this, 'links_filter' ] );
	}

	public function links_filter( array $links ): array {
		$plugin_links = [
			'<a style="color:#007050; font-weight:bold" href="' . $this->plugin_helper->generate_url_by_view( MarketingViewAction::class ) . '">' . __( 'Start here', 'woocommerce-dropshipping-xml-core' ) . '</a>',
			'<a href="' . $this->plugin_helper->generate_url_by_view( ImportManagerViewAction::class ) . '">' . __( 'Settings', 'woocommerce-dropshipping-xml-core' ) . '</a>',
			'<a href="' . __( 'https://www.wpdesk.net/docs/dropshipping-xml-woocommerce/', 'woocommerce-dropshipping-xml-core' ) . '">' . __( 'Docs', 'woocommerce-dropshipping-xml-core' ) . '</a>',
		];

		return array_merge( $plugin_links, $links );
	}
}
