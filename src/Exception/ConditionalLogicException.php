<?php
namespace WPDesk\Library\DropshippingXmlCore\Exception;

use RuntimeException;

/**
 * Class CurlHttpException.
 *
 * @package WPDesk\Library\DropshippingXmlCore\Exception\Connector
 */
class ConditionalLogicException extends RuntimeException{}
