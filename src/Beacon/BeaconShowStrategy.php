<?php
namespace WPDesk\Library\DropshippingXmlCore\Beacon;

use WPDesk\Beacon\BeaconShouldShowStrategy;

class BeaconShowStrategy implements BeaconShouldShowStrategy {

	public function shouldDisplay() {
		return true;
	}
}
