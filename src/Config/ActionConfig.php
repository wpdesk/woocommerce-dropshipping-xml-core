<?php

namespace WPDesk\Library\DropshippingXmlCore\Config;

use WPDesk\Library\DropshippingXmlCore\Infrastructure\Config\Abstraction\AbstractSingleConfig;
use WPDesk\Library\DropshippingXmlCore\Action\Cron\ImportCronAction;
use WPDesk\Library\DropshippingXmlCore\Action\Installer\PluginInstallerAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Assets\PluginAssetsLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Assets\AjaxAssetsLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Assets\WooAssetsLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Menu\AdminMenuLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Plugin\PluginLinksLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\PostType\ImportPostTypeLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Process\Form\ImportManagerFormProcessAction;
use WPDesk\Library\DropshippingXmlCore\Action\Process\ImportProcessAction;
use WPDesk\Library\DropshippingXmlCore\Action\Ajax\{
	ConvertCsvImportAjaxAction,
	ConvertXmlImportAjaxAction,
	PreviewCsvImportAjaxAction,
	PreviewXmlImportAjaxAction,
	StopImportAjaxAction,
	FileImportAjaxAction,
	PreviewVariationsAjaxAction
};
use WPDesk\Library\DropshippingXmlCore\Action\Cron\ClearTempFilesCronAction;
use WPDesk\Library\DropshippingXmlCore\Action\Process\Form\ImportSidebarFormProcessAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Product\ProductColumnLoaderAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Notification\FileLimitNotificationAction;
use WPDesk\Library\DropshippingXmlCore\Action\Loader\Assets\MenuPluginAssetsLoaderAction;

/**
 * Class ActionConfig, configuration class for actions.
 *
 * @package WPDesk\Library\DropshippingXmlCore\Config
 */
class ActionConfig extends AbstractSingleConfig {

	const ID = 'action';

	public function get(): array {
		return [
			PluginInstallerAction::class,
			FileLimitNotificationAction::class,
			FileImportAjaxAction::class,
			ConvertCsvImportAjaxAction::class,
			PreviewCsvImportAjaxAction::class,
			ConvertXmlImportAjaxAction::class,
			PreviewXmlImportAjaxAction::class,
			PreviewVariationsAjaxAction::class,
			StopImportAjaxAction::class,
			ClearTempFilesCronAction::class,
			ImportCronAction::class,
			ImportProcessAction::class,
			ImportManagerFormProcessAction::class,
			ImportSidebarFormProcessAction::class,
			AjaxAssetsLoaderAction::class,
			PluginAssetsLoaderAction::class,
			MenuPluginAssetsLoaderAction::class,
			WooAssetsLoaderAction::class,
			ImportPostTypeLoaderAction::class,
			PluginLinksLoaderAction::class,
			AdminMenuLoaderAction::class,
			ProductColumnLoaderAction::class,

		];
	}

	public function get_id(): string {
		return self::ID;
	}
}
