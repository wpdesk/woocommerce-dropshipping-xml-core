<?php

namespace WPDesk\Library\DropshippingXmlCore\Form\Fields\Field;

use WPDesk\Forms\Field\InputTextField;
use WPDesk\Library\DropshippingXmlCore\Form\Fields\Field\Sanitizer\UrlFieldSanitizer;

class InputUrlField extends InputTextField {


	public function get_sanitizer() {
		return new UrlFieldSanitizer();
	}
}
